plugins {
    java
    id("org.springframework.boot") version "2.5.1"
    id("io.spring.dependency-management") version "1.0.11.RELEASE"
}

group = "sirius"
version = "0.0.1-SNAPSHOT"

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(11))
    }
}

repositories {
    mavenCentral()
}

dependencies{
    implementation ("org.springframework.boot:spring-boot-starter-data-jdbc")
    implementation ("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation ("org.springframework.boot:spring-boot-starter-data-rest")
    implementation ("org.springframework.boot:spring-boot-starter-jersey")
    implementation ("org.springframework.boot:spring-boot-starter-web")
    implementation ("org.springframework.session:spring-session-core")
    implementation("com.fasterxml.jackson.core:jackson-core:2.12.4")

    implementation("org.liquibase:liquibase-core:4.4.3")
    implementation("org.springdoc:springdoc-openapi-ui:1.5.10")
    compileOnly ("org.projectlombok:lombok")
    runtimeOnly ("org.postgresql:postgresql")
    annotationProcessor ("org.projectlombok:lombok")
    testImplementation ("org.springframework.boot:spring-boot-starter-test")
    testImplementation ("org.testcontainers:postgresql:1.16.0")
}

tasks {
    test {
        useJUnitPlatform()
    }
}
