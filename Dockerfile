FROM adoptopenjdk:11-jre
ENV DB_HOST 34.116.131.138
ENV DB_NAME sirius6_db
ENV DB_USERNAME sirius6
ENV DB_PASSWORD YMgBJ3S

ARG JAR_FILE=build/libs/financial-tracker-0.0.1-SNAPSHOT.jar
COPY ${JAR_FILE} app.jar

ENTRYPOINT ["java","-jar","-XX:+UseSerialGC","-Xss512k","-XX:MaxRAM=256m","/app.jar"]

