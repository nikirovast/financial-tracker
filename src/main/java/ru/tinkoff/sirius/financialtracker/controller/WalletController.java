package ru.tinkoff.sirius.financialtracker.controller;

import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.sirius.financialtracker.dto.AllWalletsDTO;
import ru.tinkoff.sirius.financialtracker.dto.WalletCreateDTO;
import ru.tinkoff.sirius.financialtracker.dto.WalletDTO;
import ru.tinkoff.sirius.financialtracker.service.CentralBankInfoService;
import ru.tinkoff.sirius.financialtracker.service.WalletService;

@RestController
@RequestMapping("api/wallets")
@RequiredArgsConstructor
public class WalletController {
    private final WalletService walletService;

    @Operation(summary = "Создание нового кошелька")
    @PostMapping(
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<WalletDTO> createWallet(@RequestBody WalletCreateDTO walletCreateDTO) {
        return ResponseEntity.ok().body(walletService.createWallet(walletCreateDTO));
    }

    @Operation(summary = "Получить все кошельки")
    @GetMapping
    public AllWalletsDTO getAllWallets() {
        return walletService.getAllWallets();
    }

    @Operation(summary = "Получить кошелек по id")
    @GetMapping("/{id}")
    public ResponseEntity<WalletDTO> getWalletWithId(@PathVariable Long id) {
        return ResponseEntity.ok().body(walletService.getWalletWithId(id));
    }

    @Operation(summary = "Изменить кошелек")
    @PutMapping("/{id}")
    public ResponseEntity<WalletDTO> updateWalletWithId(@PathVariable Long id, @RequestBody WalletCreateDTO walletCreateDTO) {
        return ResponseEntity.ok().body(walletService.updateWalletWithId(id, walletCreateDTO));
    }

    @Operation(summary = "Удалить кошелек по id")
    @DeleteMapping("/{id}")
    public void deleteWalletWithId(@PathVariable Long id) {
        walletService.deleteWalletWithId(id);
    }
}
