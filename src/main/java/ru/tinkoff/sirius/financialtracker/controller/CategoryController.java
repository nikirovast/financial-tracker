package ru.tinkoff.sirius.financialtracker.controller;

import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.sirius.financialtracker.dto.CategoryCreateDTO;
import ru.tinkoff.sirius.financialtracker.dto.CategoryDTO;
import ru.tinkoff.sirius.financialtracker.service.CategoryService;
import java.util.List;

@RestController
@RequestMapping("api/categories")
@RequiredArgsConstructor
public class CategoryController {
    private final CategoryService categoryService;

    @Operation(summary = "Создание новой категории")
    @PostMapping(
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<CategoryDTO> createCategory(@RequestBody CategoryCreateDTO categoryCreateDTO) {
        return ResponseEntity.ok().body(categoryService.createCategory(categoryCreateDTO));
    }

    @Operation(summary = "Получить все категории")
    @GetMapping
    public List<CategoryDTO> getAllCategories() {
        return categoryService.getAllCategories();
    }

    @Operation(summary = "Удалить категорию")
    @DeleteMapping("/{id}")
    public void deleteCategoryWithId(@PathVariable Long id) {
        categoryService.deleteCategoryWithId(id);
    }

    @Operation(summary = "Изменить категорию")
    @PutMapping(
            value = "/{id}",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<CategoryDTO> updateCategoryWithId(@PathVariable Long id, @RequestBody CategoryCreateDTO categoryCreateDTO) {
        return ResponseEntity.ok().body(categoryService.updateCategoryWithId(id, categoryCreateDTO));
    }
}
