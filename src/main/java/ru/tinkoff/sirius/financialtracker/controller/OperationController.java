package ru.tinkoff.sirius.financialtracker.controller;

import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.sirius.financialtracker.dto.OperationCreateDTO;
import ru.tinkoff.sirius.financialtracker.dto.OperationDTO;
import ru.tinkoff.sirius.financialtracker.service.OperationService;

import java.util.List;

@RestController
@RequestMapping("api/operations")
@RequiredArgsConstructor
public class OperationController {

    private final OperationService operationService;

    @Operation(summary = "Создание новой операции")
    @PostMapping(
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<OperationDTO> createOperation(@RequestBody OperationCreateDTO operationCreateDTO) {
        return ResponseEntity.ok().body(operationService.createOperation(operationCreateDTO));
    }

    @Operation(summary = "Получить все операции")
    @GetMapping
    public List<OperationDTO> getAllOperations(@RequestParam(required = false) Long walletId ) {
        return operationService.getAllOperations(walletId);
    }

    @Operation(summary = "Получить операцию")
    @GetMapping("/{id}")
    public ResponseEntity<OperationDTO> getOperationWithId(@PathVariable Long id) {
        return ResponseEntity.ok().body(operationService.getOperationWithId(id));
    }


    @Operation(summary = "Изменить операцию")
    @PutMapping(
            value = "/{id}",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<OperationDTO> updateOperationWithId(@PathVariable Long id, @RequestBody OperationCreateDTO operationCreateDTO) {
        return ResponseEntity.ok().body(operationService.updateOperationWithId(id, operationCreateDTO));
    }

    @Operation(summary = "Удалить операцию")
    @DeleteMapping("/{id}")
    public void deleteOperationWithId(@PathVariable Long id) {
        operationService.deleteOperationWithId(id);
    }
}