package ru.tinkoff.sirius.financialtracker.controller;

import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.sirius.financialtracker.dto.CurrencyDTO;
import ru.tinkoff.sirius.financialtracker.service.CurrencyService;

import java.util.List;

@RestController
@RequestMapping("api/currencies")
@RequiredArgsConstructor
public class CurrencyController {
    private final CurrencyService currencyService;

    @Operation(summary = "Получить все валюты")
    @GetMapping
    public List<CurrencyDTO> getAllCurrencies() {
        return currencyService.getAllCurrencies();
    }
}


