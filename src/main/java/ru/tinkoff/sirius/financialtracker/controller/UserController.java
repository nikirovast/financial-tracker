package ru.tinkoff.sirius.financialtracker.controller;

import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.sirius.financialtracker.dto.UserDTO;
import ru.tinkoff.sirius.financialtracker.service.UserService;

@RestController
@RequestMapping("api/users")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;
    @Operation(summary = "Создание нового пользователя")
    @PostMapping(
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<UserDTO> createUser(@RequestBody UserDTO userDTO) {
        return ResponseEntity.ok().body(userService.createUser(userDTO));
    }
    @Operation(summary = "Удалить пользователя по id")
    @DeleteMapping("/{id}")
    public void deleteUserWithId(@PathVariable Long id) {
        userService.deleteUserWithId(id);
    }

}
