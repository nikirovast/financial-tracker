package ru.tinkoff.sirius.financialtracker.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import ru.tinkoff.sirius.financialtracker.service.CentralBankInfoService;

@Configuration
@EnableScheduling
@RequiredArgsConstructor
public class SchedulerConfig {

    private final CentralBankInfoService bankInfoService;

    // @Scheduled(cron = "30 11 * * MON-FRI")
    @Scheduled(cron = "0 30 * * * *")
    public void getCurrencies() {
        bankInfoService.getCurrencies();
    }
}
