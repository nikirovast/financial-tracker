package ru.tinkoff.sirius.financialtracker.converter;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.tinkoff.sirius.financialtracker.dto.OperationDTO;
import ru.tinkoff.sirius.financialtracker.dto.OperationCreateDTO;
import ru.tinkoff.sirius.financialtracker.model.Operation;


@Component
@RequiredArgsConstructor
public class OperationDTOToOperationConverter {

    private final CategoryDTOToCategoryConverter converter;

//    public Operation convert(OperationDTO operationDTO) {
//        return new Operation()
//                .setId(operationDTO.getId())
//                .setValue(operationDTO.getValue())
//                .setDate(operationDTO.getDate());
//    }

    public OperationDTO convert(Operation operation) {
        return new OperationDTO()
                .setId(operation.getId())
                .setValue(operation.getValue())
                .setCategoryDTO(converter.convert(operation.getCategory()))
                .setWalletId(operation.getWallet().getId())
                .setDate(operation.getDate());
    }
    public Operation convert(OperationCreateDTO operationCreateDTO) {
        return new Operation()
                .setValue(operationCreateDTO.getValue())
                .setDate(operationCreateDTO.getDate());
    }
}

