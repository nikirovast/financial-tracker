package ru.tinkoff.sirius.financialtracker.converter;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.tinkoff.sirius.financialtracker.dto.AllWalletsDTO;
import ru.tinkoff.sirius.financialtracker.dto.WalletCreateDTO;
import ru.tinkoff.sirius.financialtracker.dto.WalletDTO;
import ru.tinkoff.sirius.financialtracker.model.CategoryType;
import ru.tinkoff.sirius.financialtracker.model.Operation;
import ru.tinkoff.sirius.financialtracker.model.Wallet;
import ru.tinkoff.sirius.financialtracker.service.CentralBankInfoService;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


@Component
@RequiredArgsConstructor
public class WalletDTOToWalletConverter {
    private final CentralBankInfoService bankInfoService;

//    public Wallet convert(WalletDTO walletDTO) {
//        return new Wallet()
//                //.setId(walletDTO.getId())
//                .setName(walletDTO.getName())
//                .setMonthLimit(walletDTO.getMonthLimit())
//                .setClosed(walletDTO.isClosed());
//    }

    public Wallet convert(WalletCreateDTO walletCreateDTO) {
        return new Wallet()
                //.setId(walletDTO.getId())
                .setName(walletCreateDTO.getName())
                .setMonthLimit(walletCreateDTO.getMonthLimit())
                .setClosed(walletCreateDTO.isClosed());
    }

    public WalletDTO convert(Wallet wallet) {

        Month monthNow = LocalDateTime.now().getMonth();

        BigDecimal currBalance = BigDecimal.ZERO;
        BigDecimal monthIncome = BigDecimal.ZERO;
        BigDecimal monthExpense = BigDecimal.ZERO;

        for (Operation operation : wallet.getOperations()) {
            Date operationDate = new Date(operation.getDate());
            Month operationLocalDate = LocalDateTime
                    .ofInstant(operationDate.toInstant(), ZoneId.systemDefault())
                    .getMonth();
            if (operation.getCategory().getCategoryType() == CategoryType.income) {
                currBalance = currBalance.add(operation.getValue());
                if (monthNow == operationLocalDate) {
                    monthIncome = monthIncome.add(operation.getValue());
                }
            } else {
                if (monthNow == operationLocalDate) {
                    monthExpense = monthExpense.add(operation.getValue());
                }
                currBalance = currBalance.subtract(operation.getValue());
            }
        }
        return new WalletDTO()
                .setId(wallet.getId())
                .setName(wallet.getName())
                .setCurrency(wallet.getCurrency().getId())
                .setMonthLimit(wallet.getMonthLimit())
                .setMonthExpense(monthExpense)
                .setCurrBalance(currBalance)
                .setMonthIncome(monthIncome)
                .setClosed(wallet.isClosed());

    }


    private BigDecimal sumIncome(Wallet wallet) {
        return wallet.operations.stream()
                .filter(operation -> operation.getCategory().getCategoryType() == CategoryType.income)
                .map(Operation::getValue)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private BigDecimal sumOutcome(Wallet wallet) {
        return wallet.operations.stream()
                .filter(operation -> operation.getCategory().getCategoryType() == CategoryType.expense)
                .map(Operation::getValue)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public AllWalletsDTO convert(List<Wallet> wallets) {
        BigDecimal eurToRub = bankInfoService.getRateToRub("eur");
        BigDecimal usdToRub = bankInfoService.getRateToRub("usd");

        BigDecimal income = BigDecimal.ZERO;
        for (Wallet wallet : wallets) {
            Long currencyId = wallet.getCurrency().getId();
            if (currencyId == 1) {
                income = income.add(sumIncome(wallet));
            } else if (currencyId == 2) {
                income = income.add(sumIncome(wallet).multiply(eurToRub));
            } else {
                income = income.add(sumIncome(wallet).multiply(usdToRub));
            }
        }

        BigDecimal outcome = BigDecimal.ZERO;
        for (Wallet wallet : wallets) {
            Long currencyId = wallet.getCurrency().getId();
            if (currencyId == 1) {
                outcome = outcome.add(sumOutcome(wallet));
            } else if (currencyId == 2) {
                outcome = outcome.add(sumOutcome(wallet).multiply(eurToRub));
            } else {
                outcome = outcome.add(sumOutcome(wallet).multiply(usdToRub));
            }
        }
        List<WalletDTO> walletDTOS = wallets.stream().map(this::convert).collect(Collectors.toList());
        return new AllWalletsDTO()
                .setWallets(walletDTOS)
                .setIncome(income)
                .setExpense(outcome)
                .setBalance(income.subtract(outcome));
    }
}





