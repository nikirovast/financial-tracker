package ru.tinkoff.sirius.financialtracker.converter;

import org.springframework.stereotype.Component;
import ru.tinkoff.sirius.financialtracker.dto.CategoryCreateDTO;
import ru.tinkoff.sirius.financialtracker.dto.CategoryDTO;
import ru.tinkoff.sirius.financialtracker.model.Category;
import ru.tinkoff.sirius.financialtracker.model.User;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class CategoryDTOToCategoryConverter {
//    public Category convert(CategoryDTO categoryDTO) {
//        return new Category()
//                .setId(categoryDTO.getId())
//                .setName(categoryDTO.getName())
//                .setImage(categoryDTO.getImage())
//                .setColour(categoryDTO.getColour())
//                .setCategoryType(categoryDTO.getCategoryType());
//    }

    public CategoryDTO convert(Category category) {
        return new CategoryDTO()
                .setId(category.getId())
                .setName(category.getName())
                .setImage(category.getImage())
                .setColour(category.getColour())
                .setCategoryType(category.getCategoryType());
    }

    public Category convert(CategoryCreateDTO categoryCreateDTO) {
        return new Category()
                .setName(categoryCreateDTO.getName())
                .setImage(categoryCreateDTO.getImage())
                .setColour(categoryCreateDTO.getColour())
                .setCategoryType(categoryCreateDTO.getCategoryType());
    }

    public <I, O> List<O> convert(Collection<I> source, Function<I, O> converterFunc) {
        return source.stream()
                .map(converterFunc)
                .collect(Collectors.toList());
    }

}
