package ru.tinkoff.sirius.financialtracker.converter;


import org.springframework.stereotype.Component;
import ru.tinkoff.sirius.financialtracker.dto.UserDTO;
import ru.tinkoff.sirius.financialtracker.model.User;

@Component
public class UserDTOToUserConverter {
    public User convert(UserDTO userDTO) {
        return new User()
                .setId(userDTO.getId())
                .setEmail(userDTO.getEmail());
    }

    public UserDTO convert(User userDTO) {
        return new UserDTO()
                .setId(userDTO.getId())
                .setEmail(userDTO.getEmail());
    }
}
