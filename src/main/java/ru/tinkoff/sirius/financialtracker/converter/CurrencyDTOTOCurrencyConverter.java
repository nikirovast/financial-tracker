package ru.tinkoff.sirius.financialtracker.converter;

import org.springframework.stereotype.Component;
import ru.tinkoff.sirius.financialtracker.dto.CurrencyDTO;
import ru.tinkoff.sirius.financialtracker.model.Currency;

@Component
public class CurrencyDTOTOCurrencyConverter {
    public CurrencyDTO convert(Currency currency) {
        return new CurrencyDTO()
                .setId(currency.getId())
                .setName(currency.getName());
    }
}
