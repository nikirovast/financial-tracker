package ru.tinkoff.sirius.financialtracker.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import ru.tinkoff.sirius.financialtracker.model.CentralBankInfo;
import ru.tinkoff.sirius.financialtracker.model.CurrencyRate;
import ru.tinkoff.sirius.financialtracker.model.RateToRub;
import ru.tinkoff.sirius.financialtracker.repository.RateToRubRepository;

import java.math.BigDecimal;
import java.util.Locale;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CentralBankInfoService {
    private final RateToRubRepository repository;

    private CurrencyRate[] getInfoFromCentralBank() {
        RestTemplate restTemplate = new RestTemplate();
        CentralBankInfo response = restTemplate
                .getForEntity("https://www.cbr-xml-daily.ru/daily.xml", CentralBankInfo.class).getBody();
        if (response == null) {
            throw new RuntimeException("Couldn't get data from Central Bank");
        }
        return response.getCurrencyRates();
    }

    @Transactional
    public void getCurrencies() {
        int flag = 0;
        Optional<RateToRub> rateToRub = this.repository.findById(1L);
        RateToRub rate = (rateToRub.isEmpty()) ? new RateToRub() : rateToRub.get();
        CurrencyRate[] currencyRates = this.getInfoFromCentralBank();
        for (CurrencyRate currencyRate : currencyRates) {
            if (currencyRate.charCode.equals("USD")) {
                String value = currencyRate.value.replace(',', '.');
                rate.setUsdToRub(new BigDecimal(value));
                flag++;
            }
            if (currencyRate.charCode.equals("EUR")) {
                String value = currencyRate.value.replace(',', '.');
                rate.setEurToRub(new BigDecimal(value));
                flag++;
            }
            if (flag == 2) {
                this.repository.save(rate);
                return;
            }
        }
        throw new RuntimeException("Couldn't find eur or usd in data from Central Bank");
    }

    public BigDecimal getRateToRub(String valute) {
        Optional<RateToRub> currencies = repository.findById(1L);
        if (currencies.isEmpty()) {
            throw new RuntimeException("There is no information about rates in our db available");
        }
        RateToRub rates = currencies.get();

        if (valute.toLowerCase(Locale.ROOT).equals("eur")) {
            return rates.getEurToRub();
        }
        return rates.getUsdToRub();
    }


}
