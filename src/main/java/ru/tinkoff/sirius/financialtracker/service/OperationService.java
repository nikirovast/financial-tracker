package ru.tinkoff.sirius.financialtracker.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.sirius.financialtracker.converter.OperationDTOToOperationConverter;
import ru.tinkoff.sirius.financialtracker.dto.OperationCreateDTO;
import ru.tinkoff.sirius.financialtracker.dto.OperationDTO;
import ru.tinkoff.sirius.financialtracker.model.Operation;
import ru.tinkoff.sirius.financialtracker.model.Wallet;
import ru.tinkoff.sirius.financialtracker.repository.OperationRepository;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OperationService {

    private final OperationRepository repository;
    private final OperationDTOToOperationConverter converter;
    private final WalletService walletService;
    private final CategoryService categoryService;

    @Transactional
    public OperationDTO createOperation(OperationCreateDTO operationCreateDTO) {
        Operation operation = converter.convert(operationCreateDTO);
        operation.setWallet(walletService.getById(operationCreateDTO.getWalletId()));
        operation.setCategory(categoryService.getById(operationCreateDTO.getCategoryId()));
        return converter.convert(repository.save(operation));
    }

    @Transactional
    public OperationDTO updateOperationWithId(Long id, OperationCreateDTO operationCreateDTO) {
        Operation operation = converter.convert(operationCreateDTO);
        operation.setId(id);
        operation.setWallet(walletService.getById(operationCreateDTO.getWalletId()));
        operation.setCategory(categoryService.getById(operationCreateDTO.getCategoryId()));

        Optional<Operation> updOperation = repository.findById(id);
        if (updOperation.isEmpty()) {
            return this.createOperation(operationCreateDTO);
        }
        updOperation.get()
                .setValue(operation.getValue())
                .setDate(operation.getDate())
                .setCategory(operation.getCategory())
                .setWallet(operation.getWallet());
    //        repository.save(updOperation.get());
        return converter.convert(repository.save(updOperation.get()));
    }

    @Transactional
    public OperationDTO getOperationWithId(Long id) {
        return converter.convert(repository.getById(id));
    }

    @Transactional
    public List<OperationDTO> getAllOperations(Long walletId) {
        if (walletId == null) {
            return repository.findAll()
                    .stream().map(converter::convert)
                    .collect(Collectors.toList());
        }
        return repository.findAll()
                .stream().filter(x -> Objects.equals(x.getWallet().getId(), walletId))
                .map(converter::convert)
                .collect(Collectors.toList());
    }

    @Transactional
    public void deleteOperationWithId(Long id) {
        Operation operation = repository.getById(id);
        Wallet wallet = operation.getWallet();
        wallet.removeOperation(operation);
        repository.deleteById(id);
    }
}

