package ru.tinkoff.sirius.financialtracker.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.sirius.financialtracker.converter.CategoryDTOToCategoryConverter;
import ru.tinkoff.sirius.financialtracker.dto.CategoryCreateDTO;
import ru.tinkoff.sirius.financialtracker.dto.CategoryDTO;
import ru.tinkoff.sirius.financialtracker.model.Category;
import ru.tinkoff.sirius.financialtracker.repository.CategoryRepository;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CategoryService {

    private final CategoryRepository repository;
    private final UserService userService;
    private final CategoryDTOToCategoryConverter converter;

    @Transactional
    public CategoryDTO createCategory(CategoryCreateDTO categoryCreateDTO) {
        Category category = converter.convert(categoryCreateDTO);
        //TODO:  Add user authentication
        category.setUser(userService.getById(1L));
        return converter.convert(repository.save(category));
    }

    @Transactional
    public CategoryDTO updateCategoryWithId(Long id, CategoryCreateDTO categoryCreateDTO) {
        Category category = converter.convert(categoryCreateDTO);
        category.setId(id);

        Optional<Category> updCategory = repository.findById(id);
        if (updCategory.isEmpty()) {
            return createCategory(categoryCreateDTO);
        }
        updCategory.get()
                .setId(category.getId())
                .setName(category.getName())
                .setImage(category.getImage())
                .setCategoryType(category.getCategoryType());

        return converter.convert(repository.save(updCategory.get()));
    }

    @Transactional
    public Category getById(Long id) {
        return repository.getById(id);
    }

    @Transactional(readOnly = true)
    public List<CategoryDTO> getAllCategories() {
        return converter.convert(repository.findAll(), converter::convert);
    }

    @Transactional
    public void deleteCategoryWithId(Long id) {
        repository.deleteById(id);
    }

}


