package ru.tinkoff.sirius.financialtracker.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.sirius.financialtracker.converter.WalletDTOToWalletConverter;
import ru.tinkoff.sirius.financialtracker.dto.AllWalletsDTO;
import ru.tinkoff.sirius.financialtracker.dto.WalletCreateDTO;
import ru.tinkoff.sirius.financialtracker.dto.WalletDTO;
import ru.tinkoff.sirius.financialtracker.model.Wallet;
import ru.tinkoff.sirius.financialtracker.repository.WalletRepository;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class WalletService {

    private final WalletRepository repository;
    private final WalletDTOToWalletConverter converter;
    private final UserService userService;
    private final CurrencyService currencyService;

    @Transactional
    public WalletDTO createWallet(WalletCreateDTO walletCreateDTO) {
        Wallet wallet = converter.convert(walletCreateDTO);
        wallet.setUser(userService.getById(1L));
        Long currencyId = (walletCreateDTO.getCurrency() == null) ? 1L : walletCreateDTO.getCurrency();
        wallet.setCurrency(currencyService.getById(currencyId));

        return converter.convert(repository.save(wallet));
    }

    @Transactional(readOnly = true)
    public AllWalletsDTO getAllWallets() {
        return converter.convert(repository.findAll());
    }

    @Transactional
    public WalletDTO getWalletWithId(Long id) {
        return converter.convert(repository.getById(id));
    }

    @Transactional
    public WalletDTO updateWalletWithId(Long id, WalletCreateDTO walletCreateDTO) {

        Optional<Wallet> updWallet = repository.findById(id);
        if (updWallet.isEmpty()) {
            return this.createWallet(walletCreateDTO);
        }
        Long currencyId = (walletCreateDTO.getCurrency() == null) ? 1L : walletCreateDTO.getCurrency();
        updWallet.get()
                .setName(walletCreateDTO.getName())
                .setCurrency(currencyService.getById(currencyId))
                .setMonthLimit(walletCreateDTO.getMonthLimit())
                .setClosed(walletCreateDTO.isClosed());
        Wallet wallet = repository.save(updWallet.get());
        return converter.convert(wallet);
    }

    @Transactional
    public void deleteWalletWithId(Long id) {
        repository.deleteById(id);
    }

    @Transactional
    public Wallet getById(Long id) {
        return repository.getById(id);
    }

}
