package ru.tinkoff.sirius.financialtracker.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.sirius.financialtracker.converter.CurrencyDTOTOCurrencyConverter;
import ru.tinkoff.sirius.financialtracker.dto.CurrencyDTO;
import ru.tinkoff.sirius.financialtracker.model.Currency;
import ru.tinkoff.sirius.financialtracker.repository.CurrencyRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CurrencyService {
    private final CurrencyRepository repository;
    private final CurrencyDTOTOCurrencyConverter converter;

    @Transactional(readOnly = true)
    public List<CurrencyDTO> getAllCurrencies() {
        return repository.findAll().stream().map(converter::convert).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public Currency getById(Long id) {
        return repository.getById(id);
    }
}
