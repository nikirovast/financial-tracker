package ru.tinkoff.sirius.financialtracker.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.sirius.financialtracker.converter.UserDTOToUserConverter;
import ru.tinkoff.sirius.financialtracker.dto.UserDTO;
import ru.tinkoff.sirius.financialtracker.model.User;
import ru.tinkoff.sirius.financialtracker.repository.UserRepository;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository repository;
    private final UserDTOToUserConverter converter;

    @Transactional
    public UserDTO createUser(UserDTO userDTO) {
        return converter.convert(repository.save(converter.convert(userDTO)));
    }

    @Transactional
    public void deleteUserWithId(Long id) {
        repository.deleteById(id);
    }

    @Transactional
    public User getById(Long id) {
        return repository.getById(id);
    }
}
