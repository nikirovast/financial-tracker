package ru.tinkoff.sirius.financialtracker.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Getter
@Setter
@Accessors(chain = true)
@Table(name = "rate")
public class RateToRub {
    @Id
    @GeneratedValue(generator = "operation_seq")
    @SequenceGenerator(allocationSize = 1, name = "rate_seq", sequenceName = "rate_seq")
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "eur")
    private BigDecimal eurToRub;
    @Column(name = "usd", nullable = false)
    private BigDecimal usdToRub;
}
