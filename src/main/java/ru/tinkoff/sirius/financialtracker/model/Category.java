package ru.tinkoff.sirius.financialtracker.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
@SequenceGenerator(allocationSize = 1, name = "category_seq", sequenceName = "category_seq")
public class Category {
    @Id
    @GeneratedValue(generator = "category_seq")
    private Long id;
    private String name;
    private String image;
    private String colour;
    @JsonIgnore
    @OneToMany(mappedBy = "category", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Operation> operations = new HashSet<>();
    @Column(name = "category_type")
    private CategoryType categoryType;
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;
}
