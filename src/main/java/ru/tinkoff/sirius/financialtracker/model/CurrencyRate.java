package ru.tinkoff.sirius.financialtracker.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;

import javax.xml.bind.annotation.XmlElement;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
public class CurrencyRate {
    @XmlElement(name = "CharCode")
    public String charCode;
    @XmlElement(name = "Nominal")
    public String nominal;
    @XmlElement(name = "Value")
    public String value;
}
