package ru.tinkoff.sirius.financialtracker.model;

public enum CategoryType {
    income,
    expense
}
