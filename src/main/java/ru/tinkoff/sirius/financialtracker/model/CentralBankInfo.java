package ru.tinkoff.sirius.financialtracker.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@XmlRootElement(name = "ValCurs")
public class CentralBankInfo {
    @XmlElement(name = "Valute")
    public CurrencyRate[] currencyRates;
}
