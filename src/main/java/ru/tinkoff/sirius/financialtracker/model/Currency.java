package ru.tinkoff.sirius.financialtracker.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@Accessors(chain = true)
@SequenceGenerator(allocationSize = 1, name = "currency_seq", sequenceName = "currency_seq")
public class Currency {
    @Id
    @GeneratedValue(generator = "currency_seq")
    private Long id;
    private String name;
    @JsonIgnore
    @OneToMany(mappedBy = "currency", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Wallet> wallets = new HashSet<>();
}
