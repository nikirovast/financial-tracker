package ru.tinkoff.sirius.financialtracker.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Getter
@Setter
@Accessors(chain = true)
public class Operation {
    @JsonIgnore
    @Id
    @GeneratedValue(generator = "operation_seq")
    @SequenceGenerator(allocationSize = 1, name = "operation_seq", sequenceName = "operation_seq")
    private Long id;
    @Column(name = "value_operation")
    private BigDecimal value;
    @Column(name = "date_operation")
    private Long date;
    @ManyToOne(fetch = FetchType.LAZY)
    private Category category;
    @ManyToOne(fetch = FetchType.LAZY)
    private Wallet wallet;
}