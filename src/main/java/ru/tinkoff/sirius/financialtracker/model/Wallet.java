package ru.tinkoff.sirius.financialtracker.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@Accessors(chain = true)
public class Wallet {
    @Id
    @GeneratedValue(generator = "wallet_seq")
    @SequenceGenerator(allocationSize = 1, name = "wallet_seq", sequenceName = "wallet_seq")
    private Long id;
    private String name;
    @ManyToOne(fetch = FetchType.LAZY)
    private Currency currency;
    @Column(name = "month_limit")
    private BigDecimal monthLimit;
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;
    @OneToMany(mappedBy = "wallet", cascade = CascadeType.ALL, orphanRemoval = true)
    public Set<Operation> operations = new HashSet<>();
    @Column(name = "is_closed")
    private boolean isClosed;

    public void removeOperation(Operation operation) {
        operations.remove(operation);
        operation.setWallet(null);
    }
}
