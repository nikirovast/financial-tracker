package ru.tinkoff.sirius.financialtracker.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.tinkoff.sirius.financialtracker.model.Operation;

import java.util.Set;

public interface OperationRepository extends JpaRepository<Operation, Long> {
    Set<Operation> getOperationByCategoryId(Long category_id);
}
