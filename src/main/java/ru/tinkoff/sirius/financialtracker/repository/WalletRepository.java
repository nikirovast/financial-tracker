package ru.tinkoff.sirius.financialtracker.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.tinkoff.sirius.financialtracker.model.Wallet;

@Repository
public interface WalletRepository extends JpaRepository<Wallet, Long> {
}
