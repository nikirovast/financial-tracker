package ru.tinkoff.sirius.financialtracker.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.tinkoff.sirius.financialtracker.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
}
