package ru.tinkoff.sirius.financialtracker.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.tinkoff.sirius.financialtracker.model.Currency;

public interface CurrencyRepository extends JpaRepository<Currency, Long> {
}
