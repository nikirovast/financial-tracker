package ru.tinkoff.sirius.financialtracker.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.tinkoff.sirius.financialtracker.model.RateToRub;

public interface RateToRubRepository extends JpaRepository<RateToRub, Long> {
}
