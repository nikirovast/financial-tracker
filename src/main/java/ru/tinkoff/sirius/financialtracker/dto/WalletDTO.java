package ru.tinkoff.sirius.financialtracker.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

@Getter
@Setter
@Accessors(chain = true)
@Schema(description = "Кошелек")
public class WalletDTO {
    private Long id;
    @NotBlank
    @Schema(description = "Название кошелька",  example = "Каждодневные траты", required = true)
    private String name;
    @Schema(description = "ID валюты")
    private Long currency;
    @Schema(description = "Лимит по кошельку на месяц")
    private BigDecimal monthLimit;
    @Schema(description = "Текущий баланс за все время")
    private BigDecimal currBalance;
    @Schema(description = "Пополнения за месяц")
    private BigDecimal monthIncome;
    @Schema(description = "Траты за месяц")
    private BigDecimal monthExpense;
    @Schema(description = "Если кошелек закрытый, то true; иначе - false")
    private boolean isClosed;
}
