package ru.tinkoff.sirius.financialtracker.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
public class AllWalletsDTO {
    List<WalletDTO> wallets;
    @Schema(description = "Баланс на всех кошельках")
    private BigDecimal balance;
    @Schema(description = "Пополнения на всех кошельках")
    private BigDecimal income;
    @Schema(description = "Расходы на всех кошельках")
    private BigDecimal expense;
}
