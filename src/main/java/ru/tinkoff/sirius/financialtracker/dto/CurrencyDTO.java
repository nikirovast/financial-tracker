package ru.tinkoff.sirius.financialtracker.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@Schema(description = "Валюта")
public class CurrencyDTO {
    private Long id;
    @Schema(description = "Название валют", required = true)
    private String name;
}
