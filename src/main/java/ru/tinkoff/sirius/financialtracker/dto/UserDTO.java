package ru.tinkoff.sirius.financialtracker.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@Accessors(chain = true)
@Schema(description = "Пользователь")
public class UserDTO {
    private Long id;
    @Schema(description = "Email", required = true)
    @NotBlank
    private String email;
}
