package ru.tinkoff.sirius.financialtracker.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.experimental.Accessors;
import lombok.Getter;
import lombok.Setter;


import java.math.BigDecimal;

@Getter
@Setter
@Accessors(chain = true)
@Schema(description = "Операция")
public class OperationCreateDTO {
    //private Long id;
    @Schema(description = "Величина операции")
    private BigDecimal value;
    @Schema(description = "Дата операции")
    private Long date;
    @Schema(description = "Категория операции")
    private Long categoryId;
    @Schema(description = "ID кошелька")
    private Long walletId;
}