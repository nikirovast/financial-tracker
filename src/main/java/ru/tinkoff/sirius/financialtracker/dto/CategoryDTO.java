package ru.tinkoff.sirius.financialtracker.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.tinkoff.sirius.financialtracker.model.CategoryType;

import javax.persistence.Column;
import javax.validation.constraints.NotBlank;


@Getter
@Setter
@Accessors(chain = true)
@Schema(description = "Категория")
public class CategoryDTO {
    private Long id;
    @Schema(description = "Название категории", example = "Супермаркет", required = true)
    @NotBlank
    private String name;
    @Schema(description = "Картинка для картинки категории")
    private String image;
    @Schema(description = "Цвет картинки для картинки категории")
    private String colour;
    @Column(name = "type_operation")
    private CategoryType categoryType;
}
