package ru.tinkoff.sirius.financialtracker.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

@Getter
@Setter
@Accessors(chain = true)
@Schema(description = "Кошелек")
public class WalletCreateDTO {
    @NotBlank
    @Schema(description = "Название кошелька",  example = "Каждодневные траты", required = true)
    private String name;
    @Schema(description = "ID валюты")
    private Long currency;
    @Schema(description = "Лимит по кошельку на месяц")
    private BigDecimal monthLimit;
    @Schema(description = "Если кошелек закрытый, то true; иначе - false")
    private boolean isClosed;
}
